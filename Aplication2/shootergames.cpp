#include "shootergames.h"
#include <string>
#include <istream>
#include <ostream>
#include "ShooterRepo.h"
using namespace std;
ShooterGames::ShooterGames()
{
    this->name = "";
    this->prod = "";
    this->AvgAge = 0;
    this->year = 0;
    this->memory = 0;
}

ShooterGames::ShooterGames(string name, string prod, int AvgAge, int year, int memory)
{
    this->name = name;
    this->prod = prod;
    this->AvgAge = AvgAge;
    this->year = year;
    this->memory = memory;
}
ostream& operator<<(ostream& os, const ShooterGames& G) {
    os << G.name << " " << G.prod <<" "<<G.AvgAge<<" "<<G.year<<" "<<G.memory<<endl;
    return os;

}

istream& operator>>(istream& is, ShooterGames& G) {
    is >> G.name >> G.prod >> G.AvgAge >> G.year >> G.memory;
    return is;
}
