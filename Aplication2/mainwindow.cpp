#include "mainwindow.h"
#include <qboxlayout.h>
#include <qlabel.h>
#include <qfont.h>
#include <qlineedit.h>
#include <qcombobox.h>
#include <qtextedit.h>
#include <qcoreapplication.h>
#include <qvector.h>
#include <qdebug.h>


MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) {
    QWidget* centralWidget = new QWidget(this);

    //QHBoxLayout* topPart = new QHBoxLayout;
    QVBoxLayout* finalLayout   = new QVBoxLayout;
    QHBoxLayout* middleLayout  = new QHBoxLayout;
    QVBoxLayout* buttonLayout  = new QVBoxLayout;
    QVBoxLayout* masterLayout  = new QVBoxLayout;
    QVBoxLayout* currentLayout = new QVBoxLayout;
    QVBoxLayout* contentLayout = new QVBoxLayout;

    QString freegame = "Valorant,Riot,12+,2019,0;";
    QString pricedgame ="Call of Duty Modern Warfare 3,Infinity Games,18+,2011,300;";

    QLabel* title = new QLabel;
    title->setText("ShooterGames!");
    title->setAlignment(Qt::AlignCenter);
    QFont font;
    font.setPointSize(16);
    title->setStyleSheet("QLabel { color : black; }");
    title->setFont(font);

    m_displayButton = new QPushButton("&Display", centralWidget);
    m_addButton = new QPushButton("&Add", centralWidget);
    m_deleteButton = new QPushButton("&Remove", centralWidget);
    m_newButton = new QPushButton("&Exit", centralWidget);


    buttonLayout->addSpacing(28);
    buttonLayout->addWidget(m_displayButton);
    buttonLayout->addWidget(m_addButton);
    buttonLayout->addWidget(m_deleteButton);
    buttonLayout->addWidget(m_newButton);
    buttonLayout->addStretch(20);


    m_masterList = new QListWidget(centralWidget);
    m_masterList->addItem(freegame);
    m_currentList = new QListWidget(centralWidget);
    m_currentList->addItem(pricedgame);

    QLabel* masterLabel = new QLabel("Free Games ", centralWidget);
    masterLabel->setAlignment(Qt::AlignCenter);
    masterLayout->addWidget(masterLabel);
    masterLayout->addWidget(m_masterList);

    QLabel* currentLabel = new QLabel("Priced Games ", centralWidget);
    currentLabel->setAlignment(Qt::AlignCenter);
    currentLayout->addWidget(currentLabel);
    currentLayout->addWidget(m_currentList);

    QLabel* contentLabel = new QLabel("Shooters list", centralWidget);
    contentLabel->setAlignment(Qt::AlignCenter);
    m_contentList = new QListWidget;
    m_contentList->addItem(freegame);
    m_contentList->addItem(pricedgame);
    contentLayout->addWidget(contentLabel);
    contentLayout->addWidget(m_contentList);

    middleLayout->addLayout(masterLayout);
    middleLayout->addLayout(buttonLayout);
    middleLayout->addLayout(currentLayout);

    finalLayout->addWidget(title);
    finalLayout->addLayout(middleLayout);
    finalLayout->addLayout(contentLayout);



    centralWidget->setLayout(finalLayout);
    this->setCentralWidget(centralWidget);


}

void MainWindow::onCopyPress() {
    qDebug() << "Pressed Copy";
}

MainWindow::~MainWindow()
{
}

void MainWindow::keyPressEvent(QKeyEvent* event) {
    if (event->type() == QKeyEvent::KeyPress)
        if (event->matches(QKeySequence::Copy)) {  // verifica practic CTRL+C aka shortcut-ul pt copy
            QCoreApplication::quit();
        }
}
