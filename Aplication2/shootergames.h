#pragma once
#include <istream>
#include <ostream>
#include <fstream>
#include <string>

using namespace std;
class ShooterGames
{
private:
    string name;
    string prod;
    int AvgAge;
    int year;
    int memory;
    int ID;



public:
    /// A base class with another 2 derived classes
    /// @param name is the name of the game
    /// @param prod is the name of the producers that made the game
    /// @param AvgAge is for the average age of people that can play
    /// @param year is for the year that the game was made
    /// @param memory is the capacity of the game
    ShooterGames();
    ShooterGames(string name, string prod, int AvgAge, int year, int memory);
    void display();
    friend ostream& operator<<(ostream& os, const ShooterGames&);
    friend istream& operator>>(istream& is, ShooterGames&);
    bool operator==(const ShooterGames& s);

     void setName(string newName) {
        this->name = newName;
    }
     void setProd(string newProd) {
        this->prod = newProd;
    }
     void setAvgAge(int newAvgAge) {
        this->AvgAge = newAvgAge;
    }
     void setMemory(int newMemory) {
        this->memory = newMemory;
    }
     void setID(int newID) {
        this->ID = newID;
    }
    string getName() const {
        return this->name;
    }
    string getProd() const {
        return this->prod;
    }
    int getAvgAge() const {
        return this->AvgAge;
    }

    int getID() const {
        return this->ID;
    }
    int getMemory() const {
        return this->memory;
    }
     ~ShooterGames() = default;

};
