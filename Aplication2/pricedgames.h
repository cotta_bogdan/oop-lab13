#pragma once
#include "shootergames.h"
class PricedGames :public ShooterGames
{
private:
    int price;





public:
    /// A derived class from ShooterGames
    /// It has 1 new param
    /// @param price means the price of a game
    PricedGames();
    PricedGames(string name, string prod, int AvgAge, int year, int memory, int price);
    void display();

     void setPrice(int newPrice) {
        this->price = newPrice;
    }
     int getPrice() const {
         return this->price;
     }
     ~PricedGames() = default;
};
