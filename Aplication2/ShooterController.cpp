#include "ShooterController.h"
#include"freegames.h"
#include "pricedgames.h"
#include "iostream"
using namespace std;

Controller::Controller(Repository& r) {
    this->repo = r;
}

vector<ShooterGames*> Controller::getAllData() const {
    return repo.getAllData();
}

void Controller::addShooterGames(string name, string prod, int AvgAge,int year, int memory,int valability,int price,string type) {
    ShooterGames* newShooterGamesObject = nullptr;
    if (type == "FreeGames") {
        newShooterGamesObject = new FreeGames( name,  prod,  AvgAge, year,  memory,  valability);
        this->repo.addShooterGames(newShooterGamesObject);
    }
    else if (type == "PricedGames") {
        newShooterGamesObject = new PricedGames(name,prod ,AvgAge ,year,memory,price);
        this->repo.addShooterGames(newShooterGamesObject);
    }

    
}

void Controller::displayAll(){
    repo.display(std::cout, [](ShooterGames* s)->bool {return true; });
}

void Controller::removeGame(string name){
    this->repo.removeByName(name);
}
