#include "mainwindow.h"
#include <qdir.h>
#include <qstring.h>
#include <QObject>
#include <QStandardPaths>
#include <QString>
#include <QDebug>   // this is cout
#include <qicon.h>
#include <QtWidgets/QApplication>


int main(int argc, char* argv[]) {
    QApplication a(argc, argv);

    MainWindow w;
    w.setWindowTitle("InventoryOfShooters");

    QIcon picture("Pictures\saveImage.png");
    w.setWindowIcon(picture);
    w.show();
    return a.exec();
}
