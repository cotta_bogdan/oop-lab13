#pragma once

#include "ShooterRepo.h"
#include "shootergames.h"
#include <string>

class Controller {
private:
    Repository repo;
public:
    Controller() = default;

    Controller(Repository& r);

    ~Controller() = default;

    vector<ShooterGames*> getAllData() const;

  
    void addShooterGames(string name, string prod, int AvgAge, int year, int memory, int valability, int price,string type);

    void displayAll();
    void removeGame(string name);

    void EditElementByName(int newProd, int newAvgAge, int newMemory);;

    int sizeController();
};
