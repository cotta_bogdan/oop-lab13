#include "ShooterRepo.h"
#include<vector>
#include"shootergames.h"
#include "freegames.h"
#include "pricedgames.h"

#include <string>



void Repository::addShooterGames(ShooterGames* newShooterGames) {

    this->data.push_back(newShooterGames);
   
}

vector<ShooterGames*> Repository::getAllData() const {
    return this->data;
}

  
void Repository::EditElementByName(string name, string prod, int AvgAge, int memory) {
        for (ShooterGames* s : this->data) 
        {
            if (s->getName() == name)
            {
                s->setProd(prod);
                s->setAvgAge(AvgAge);
                s->setMemory(memory);
            }
        }
 }

ostream& Repository::display(ostream& os, bool(*filterFunction)(ShooterGames* s))
{
    os << "List of ALL shooter games are : \n";
    for (auto i = 0; i < data.size(); i++)
        if (filterFunction(data[i]) == true)
            os << *data[i];
    return os;
}
void Repository::displayAll()
{
    display(std::cout, [](ShooterGames* s)->bool {return true; });

}
int Repository::sizeRepo() {
    return data.size();
}

void Repository::removeByName(string name) {
    int pos = -1;
    for (int i = 0; i < data.size() && pos == -1; i++) 
    {
        if (data[i]->getName() == name) {
            pos = i;
        }
    }
    if (pos != -1) {
        data.erase(data.begin() + pos);
    }
}


