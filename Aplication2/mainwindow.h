#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <qpushbutton.h>
#include <qlistwidget.h>
#include <qradiobutton.h>
#include <qmainwindow.h>
#include <QKeyEvent>

class MainWindow :public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    void keyPressEvent(QKeyEvent* event);
    ~MainWindow();

public slots:
    void onCopyPress();

private:
    QPushButton* m_displayButton;
    QPushButton* m_addButton;
    QPushButton* m_deleteButton;
    QPushButton* m_newButton;


    QListWidget* m_masterList;
    QListWidget* m_currentList;
    QListWidget* m_contentList;

};
#endif // MAINWINDOW_H
