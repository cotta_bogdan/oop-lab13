#pragma once
#include <vector>
#include "shootergames.h"
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

class Repository {
private:
	vector<ShooterGames*>data;
	string fileNameIn;
	string fileNameOut;
public:

	~Repository() = default;
	vector<ShooterGames*>getAllData() const;
	void addShooterGames(ShooterGames* newShooterGames);

	void removeByName(string name);

	void EditElementByName(string name, string prod, int AvgAge, int memory);

	ostream& display(ostream& os, bool(*filterFunction)(ShooterGames* s));
	void displayAll();
	int sizeRepo();





};
